User's Guide
============

The core component of ``http-hmac`` is the hasher,
:mod:`http_hmac.hash`, that is responsible for the hashing and the
verification of HTTP payloads.

The first thing you need to do is to provide both the server and the
client a shared secret key; in the following examples it is assumed
that this secret key is stored in the file :file:`/etc/hmac/secret`.

urllib2 handler
---------------

:mod:`http_hmac.handler` contains an handler for :mod:`urllib2` that
can be used to write simple clients::

  import urllib2
  from http_hmac.handler import HttpHmacHandler
  from http_hmac.hash import HttpHmac

  secret = '...' # the contents of /etc/hmac/secret
  hmac = HttpHmac(secret)
  handler = HttpHmacHandler(hmac)

  opener = urllib2.build_opener(handler)
  opener.open('http://www.example.com/protected')

Flask Support
-------------

:mod:`http_hmac.flask_support` contains the decorator :func:`hmac_auth`
that can be used to provide authentication to routes::

  from flask import Flask
  from http_hmac.hash import HttpHmac
  from http_hmac.flask_support import hmac_auth

  app = Flask(__name__)

  secret = '...' # the contents of /etc/hmac/secret
  app.http_hmac = HttpHmac(secret)

  @app.route('/')
  @hmac_auth
  def index():
      return "Hello World"

WSGI Middleware
---------------

:mod:`http_hmac.middleware` contains the class
:class:`HttpHmacAuthMiddleware` that can be used with any existing
`WSGI` application::

  from http_hmac.hash import HttpHmac
  from http_hmac.middleware import HttpHmacAuthMiddleware
  from myapp import app

  secret = '...' # the contents of /etc/hmac/secret
  hmac = HttpHmac(secret)
  application = HttpHmacAuthMiddleware(app, hmac)
