API
===

Hasher
------

.. autoclass:: http_hmac.hash.HttpHmac
   :members:

   You should create a instance of this class in your application
   startup, passing it the shared secret key; usually you won't
   interact directly with this object.


urllib2 Handler
---------------

.. module:: http_hmac.handler

.. class:: HttpHmacHandler(hmac)

   A :class:`urllib2.BaseHandler` subclass with `HMAC`_ signing
   support.

   :param hmac: a :class:`http_hmac.hash.HttpHmac` instance.


Flask Decorator
---------------

.. module:: http_hmac.flask_support

To use the `HMAC`_ signer in a `Flask`_ application you will need to
create an :attr:`app.http_hmac` attribute with an instance of
:class:`http_hmac.hash.HttpHmac` which has been passed the shared
secret key.

.. decorator:: hmac_auth

   Use this decorator to protect your application `routes`.

   Example::

	 @app.route('/protected')
	 @hmac_auth
	 def protected():
		return "Hello World"

WSGI Middleware
---------------

.. module:: http_hmac.middleware

.. class:: HttpHmacAuthMiddleware(app, hmac)

   WSGI Middleware support.

   :param app: a WSGI application
   :param hmac: the shared secret key


.. _HMAC: http://en.wikipedia.org/wiki/HMAC
.. _Flask: http://flask.pocoo.org/
