#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name='http_hmac',
    version='0.1.1',
    description='HMAC Authentication helpers for HTTP',
    author='ale',
    author_email='ale@incal.net',
    url='http://git.autistici.org/p/http-hmac',
    packages=find_packages(),
    install_requires=['werkzeug'],
    setup_requires=[],
    zip_safe=True,
    entry_points={},
    )
