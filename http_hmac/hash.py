import hmac
import hashlib
import time
import urlparse
from datetime import datetime
try:
    from werkzeug.http import parse_date
except ImportError:
    from werkzeug.utils import parse_date

DEFAULT_TIME_SKEW = 600


def normalize_resource_url(u):
    """Normalize a request URL (returns just path and query string)."""
    parts = urlparse.urlsplit(u)
    if parts.query:
        return '%s?%s' % (parts.path, parts.query)
    else:
        return parts.path


def timediff(a, b):
    """Return difference in seconds between datetime objects."""
    return (time.mktime(a.utctimetuple()) - time.mktime(b.utctimetuple()))


class HttpHmac(object):
    """Compute and verify an HMAC signature for a given payload

    :param key: key for the keyed hash object
    :param time_skew: maximum number of time skew seconds for a signature to
                      be valid
    :type time_skew: seconds
    """
    def __init__(self, key, time_skew=DEFAULT_TIME_SKEW):
        self.key = key
        self.time_skew = time_skew

    def hash_contents(self, contents):
        """Return a base64-encoded HMAC digest of the given contents (a list)."""
        msg = '\n'.join(['' if x is None else x for x in contents])
        digest = hmac.new(self.key,
                          msg=msg,
                          digestmod=hashlib.sha1
                          ).digest()
        return digest.encode('base64').rstrip()

    def get_content_md5(self, content):
        """Return a hex-encoded MD5 digest of a string."""
        return hashlib.md5('' if content is None else content).hexdigest()

    def get_signature(self, verb, rsrc_url, content_md5, content_type, date):
        """Return the signature for an HTTP request."""
        rsrc_url = normalize_resource_url(rsrc_url)
        return self.hash_contents([verb, content_md5, content_type,
                                   date, None, rsrc_url])

    def verify_request(self, verb, rsrc_url, body, req_content_md5, 
                       req_content_type, req_date, req_signature):
        """Returns True if the request matches the signature."""
        content_md5 = self.get_content_md5(body) if body else None
        signature = self.get_signature(verb, rsrc_url, content_md5,
                                       req_content_type, req_date)

        delta_t = 0
        if req_date:
            delta_t = abs(timediff(datetime.utcnow(), parse_date(req_date)))

        return (content_md5 == req_content_md5
                and signature == req_signature
                and delta_t < self.time_skew)


