import urllib2
try:
    from werkzeug.http import http_date
except ImportError:
    from werkzeug.utils import http_date


class HttpHmacHandler(urllib2.BaseHandler):

    # Run the handler _after_ the default ones.
    handler_order = 900

    def __init__(self, hmac):
        self._hmac = hmac

    def http_request(self, request):
        # Only add a 'Content-Md5' header if there is a request body.
        if request.has_data():
            content_md5 = self._hmac.get_content_md5(request.get_data())
            request.add_header('Content-Md5', content_md5)
        else:
            content_md5 = None

        # Set the 'Date' header if not already present.
        if not request.has_header('Date'):
            date = http_date()
            request.add_header('Date', date)
        else:
            date = request.get_header('Date')

        # Compute the request signature.
        signature = self._hmac.get_signature(
            request.get_method(),
            request.get_full_url(),
            content_md5,
            request.get_header('Content-type', 'text/plain'),
            date)
        request.add_header('HMAC-Auth', signature)

        return request

    https_request = http_request
