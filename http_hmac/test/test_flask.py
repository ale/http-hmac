import unittest
import urllib
from flask import Flask, request

from http_hmac.hash import HttpHmac
from http_hmac.handler import http_date
from http_hmac.flask_support import hmac_auth, wsgi_copy_body


app = Flask(__name__)
app.wsgi_app = wsgi_copy_body(app.wsgi_app)

@app.route('/', methods=('GET', 'POST'))
@hmac_auth
def index():
    return 'ok'


class FlaskTest(unittest.TestCase):

    def setUp(self):
        self.hmac = HttpHmac('secret')
        app.config['TESTING'] = True
        app.http_hmac = self.hmac
        self.c = app.test_client()

    def _make_post_request(self, url, data, sig=None):
        enc = urllib.urlencode(data)
        date = http_date()
        content_md5 = self.hmac.get_content_md5(enc)
        content_type = 'application/x-form-www-urlencoded'
        if not sig:
            sig = self.hmac.get_signature(
                'POST', '/', content_md5, content_type, date)
        return self.c.post(url, data=enc, headers={
            'Date': date,
            'Content-Md5': content_md5,
            'Content-Type': content_type,
            'HMAC-Auth': sig,
        })

    def test_post_request(self):
        resp = self._make_post_request('/', {'a': '42'})
        self.assertEquals(200, resp.status_code)

    def test_post_request_bad_signature(self):
        resp = self._make_post_request('/', {'a': '42'}, sig='bad signature')
        self.assertEquals(403, resp.status_code)

    def test_post_request_unsigned(self):
        resp = self.c.post('/', data={'a': '42'})
        self.assertEquals(403, resp.status_code)
