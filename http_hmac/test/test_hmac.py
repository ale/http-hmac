import pprint
import threading
import unittest
import urllib2
from wsgiref.simple_server import make_server
from http_hmac.hash import HttpHmac
from http_hmac.handler import HttpHmacHandler
from http_hmac.middleware import HttpHmacAuthMiddleware


_port = 10001
def get_port():
    global _port
    _port += 1
    return _port


class TestServerApp(object):

    def __init__(self):
        self.requests = []

    def __call__(self, environ, start_response):
        self.requests.append(dict(environ))
        start_response('200 Ok', [])
        return ['OK']


class HttpServerThread(threading.Thread):

    def __init__(self, app, port):
        threading.Thread.__init__(self)
        self.server = make_server('127.0.0.1', port, app)

    def stop(self):
        self.server.shutdown()
        self.join()

    def run(self):
        self.server.serve_forever(poll_interval=0.1)


class HmacIntegrationTest(unittest.TestCase):

    def setUp(self):        
        self.hmac = HttpHmac('secret')
        self.opener = urllib2.build_opener(
            HttpHmacHandler(self.hmac))

        port = get_port()
        self.base_url = 'http://localhost:%d' % port
        self.server_app = TestServerApp()
        self.server_thread = HttpServerThread(
            HttpHmacAuthMiddleware(self.server_app, self.hmac),
            port)
        self.server_thread.start()

    def tearDown(self):
        self.server_thread.stop()

    def test_plain_request(self):
        z = self.opener.open(self.base_url + '/').read()
        self.assertEquals('OK', z)

    def test_request_with_query_args(self):
        z = self.opener.open(self.base_url + '/?a=42&b=off').read()
        self.assertEquals('OK', z)

    def test_post_request(self):
        z = self.opener.open(self.base_url + '/post-request',
                             'a=1&b=2').read()
        self.assertEquals('OK', z)

    def test_fail_unsigned_request(self):
        self.assertRaises(urllib2.HTTPError,
                          urllib2.urlopen,
                          self.base_url + '/')

    def test_fail_bad_secret(self):
        opener = urllib2.build_opener(
            HttpHmacHandler(HttpHmac('bad_secret')))
        self.assertRaises(urllib2.HTTPError,
                          opener.open,
                          self.base_url + '/')


if __name__ == '__main__':
    unittest.main()

