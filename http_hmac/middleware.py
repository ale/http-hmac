from cStringIO import StringIO


class HttpHmacAuthMiddleware(object):

    def __init__(self, app, hmac):
        self.app = app
        self.hmac = hmac

    def _unauthorized(self, environ, start_response):
        start_response('403 Unauthorized',
                       [('Content-Type', 'text/html')])
        return ['<h1>Unauthorized</h1>']

    def __call__(self, environ, start_response):
        # We need to read all data from the WSGI request, but still make
        # it available downstream, so we put it in a StringIO buffer.
        length = int(environ.get('CONTENT_LENGTH') or '0')
        data = environ['wsgi.input'].read(length)
        environ['wsgi.input'] = StringIO(data)

        # Re-assemble the URI from the environment.
        url = environ['PATH_INFO']
        if environ.get('QUERY_STRING'):
            url = '%s?%s' % (url, environ['QUERY_STRING'])

        # Check for a valid signature.
        if not self.hmac.verify_request(
            environ['REQUEST_METHOD'],
            url,
            data,
            environ.get('HTTP_CONTENT_MD5'),
            environ.get('CONTENT_TYPE'),
            environ.get('HTTP_DATE'),
            environ.get('HTTP_HMAC_AUTH')):
            return self._unauthorized(environ, start_response)

        return self.app(environ, start_response)

