"""Flask support utilities for http-hmac auth.

A decorator is provided to enable http-hmac authentication on
application methods.

In order to use this functionality, you'll need to wrap your
application with the `wsgi_copy_body` WSGI middleware, otherwise the
decorator will not have access to the request data on requests whose
Content-Type is parsed by Flask itself (most importantly,
application/x-form-www-urlencoded).

Example setup::

    from http_hmac.hash import HttpHmac
    from http_hmac.flask_support import hmac_auth, wsgi_copy_body

    app.http_hmac = HttpHmac(...)
    app.wsgi_app = wsgi_copy_body(app.wsgi_app)

    @app.route('/')
    @hmac_auth
    def index():
        # do something

"""

import functools
import cStringIO
from flask import current_app, request, abort


def hmac_auth(fn):
    @functools.wraps(fn)
    def _hmac_auth(*args, **kwargs):
        # Check if the app is set up at all.
        if hasattr(current_app, 'http_hmac'):
            hmac = current_app.http_hmac

            # Check for a valid signature.
            if not hmac.verify_request(
                request.method,
                request.path,
                request.environ.get('body_copy', ''),
                request.headers.get('Content-Md5'),
                request.headers.get('Content-Type'),
                request.headers.get('Date'),
                request.headers.get('HMAC-Auth')):
                abort(403)

        return fn(*args, **kwargs)

    return _hmac_auth


def wsgi_copy_body(application):
    def _copy_body(environ, start_response):
        length = environ.get('CONTENT_LENGTH', '0')
        length = 0 if length == '' else int(length)
        body = environ['wsgi.input'].read(length)
        environ['body_copy'] = body
        environ['wsgi.input'] = cStringIO.StringIO(body)
        return application(environ, start_response)
    return _copy_body
