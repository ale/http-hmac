http-hmac
---------

``http-hmac`` is a Python implementation of `HMAC`_ based
authentication of HTTP requests; with `HMAC`_ authentication a client
signs each HTTP request with a secret key shared with the server. This
provides authentication and data integrity without the need to set up
SSL.

Example Usage
'''''''''''''

The library can be easily hooked in existing code using
:mod:`urllib2`, for example::

  import urllib2
  from http_hmac.handler import HttpHmacHandler
  from http_hmac.hash import HttpHmac

  secret = 'my secret key'
  hmac = HttpHmac(secret)
  handler = HttpHmacHandler(hmac)

  opener = urllib2.build_opener(handler)
  opener.open(a_url)

For the server side the library includes a `WSGI` middleware and a
`decorator` for the `Flask`_ micro-framework.

.. seealso:: module :mod:`urllib2`.

.. _HMAC: http://en.wikipedia.org/wiki/HMAC
.. _Flask: http://flask.pocoo.org/
